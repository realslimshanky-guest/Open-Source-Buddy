# Open Source Buddy

This software is an assignment task "Wizard/GUI helping students/interns apply and get started" for GSoC 2018 under Debian.

**Updates** - My proposal has been [selected](https://summerofcode.withgoogle.com/projects/#5056989357408256) for GSoC!

## Blog about My GSoC Project and Appliciation Task

[Blog 1 - From Preparations to Debian to Proposal](https://blog.shanky.xyz/gsoc-2018-blog-1-from-preparations-to-debian-proposal.html)

[Blog 2 - The Application Task and Results](https://blog.shanky.xyz/gsoc-2018-blog-2-the-application-task-and-results.html)

### What have I done for the assignment

I've used Python-GnuPG to demonstrate Key Pair management using the GUI. Current functionalities available are
- Key Creation
- Displaying Available Keys
- Displaying Public Keys
- Encrypting Text
- Decrypting Text

### Checkout the Demo

Open [`/demo.mp4`](/demo.mp4) to watch the recording of demonstration.

### How to deploy

##### Clone this Repository

git clone 
cd Open-Source-Buddy/

##### Setup Virtual Environment

pip install pipenv
pipenv --three

##### Installing Dependencies

pipenv install --skip-lock

##### Running the Application

pipenv run python app.py

### How to test

##### Testing codebase with PEP8 guidelines

pylint app.py

