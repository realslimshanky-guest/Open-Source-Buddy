#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""pylint: --*-naming-style=camelCase"""

import re
import sys
from datetime import datetime

import gnupg
from PyQt5.QtWidgets import (# pylint: disable-msg=E0611
    QWidget, QHBoxLayout, QVBoxLayout,
    QPushButton, QApplication, QLabel,
    QGridLayout, QDialog, QLineEdit,
    QPlainTextEdit
    )
from PyQt5.QtCore import Qt # pylint: disable-msg=E0611

class OpenSourceBuddy(QWidget):
    """
    Root class which holds parent widget.
    """

    # pylint: disable=too-many-instance-attributes
    # pylint: disable=too-many-statements

    def __init__(self):
        super().__init__()

        # Initializing home folder for GnuPG
        self.gpg = gnupg.GPG(gnupghome="./keys/")

        # Initializing regex to validate email addresses
        self.regex = re.compile(r"[^@]+@[^@]+\.[^@]+")

        # Initializing variables to be used in other methods
        self.nameEdit = None
        self.passEdit = None
        self.decryptedText = None
        self.keyToUseForEncryption = None
        self.passPhraseEdit = None
        self.emailEdit = None
        self.textToDecrypt = None
        self.textToEncrypt = None
        self.encryptedText = None

        self.initUI()


    def initUI(self):
        """
        Parent widget containing the first window that loads up on
        pipenv run python app.py
        """
        vbox1 = QVBoxLayout()
        navgrid = QGridLayout()
        encryptiongrid = QGridLayout()

        gridwidget = QWidget()
        gridwidget.setProperty("navgridwidget", True)
        navgrid.addWidget(gridwidget, 0, 0, 10, 0)

        self.navlabel1 = QLabel("Roadmap")
        self.navlabel1.setProperty("navTitle", True)
        self.navlabel1.setAlignment(Qt.AlignCenter)
        navgrid.addWidget(self.navlabel1, 0, 0)

        self.btn1 = QPushButton('Introduction')
        self.btn1.setProperty("navButton", True)
        self.btn1.clicked.connect(self.showDevelopMessage)
        navgrid.addWidget(self.btn1, 2, 0)

        self.btn2 = QPushButton('Communication')
        self.btn2.setProperty("navButton", True)
        self.btn2.clicked.connect(self.showDevelopMessage)
        navgrid.addWidget(self.btn2, 3, 0)

        self.btn3 = QPushButton('CLI')
        self.btn3.setProperty("navButton", True)
        self.btn3.clicked.connect(self.showDevelopMessage)
        navgrid.addWidget(self.btn3, 4, 0)

        self.btn4 = QPushButton('Blog')
        self.btn4.setProperty("navButton", True)
        self.btn4.clicked.connect(self.showDevelopMessage)
        navgrid.addWidget(self.btn4, 5, 0)

        self.btn5 = QPushButton('Version Control')
        self.btn5.setProperty("navButton", True)
        self.btn5.clicked.connect(self.showDevelopMessage)
        navgrid.addWidget(self.btn5, 6, 0)

        self.btn6 = QPushButton('Encryption')
        self.btn6.setProperty("navButton", True)
        self.btn6.clicked.connect(self.showDevelopMessage)
        navgrid.addWidget(self.btn6, 7, 0)

        self.btn7 = QPushButton('Way Ahead')
        self.btn7.setProperty("navButton", True)
        self.btn7.clicked.connect(self.showDevelopMessage)
        navgrid.addWidget(self.btn7, 8, 0)

        vbox1.addLayout(navgrid)

        vbox2 = QVBoxLayout()

        screengridwidget = QWidget()
        screengridwidget.setProperty("screengridwidget", True)
        encryptiongrid.addWidget(screengridwidget, 0, 0, 9, 0)

        self.screenlabel1 = QLabel("Encryption")
        self.screenlabel1.setProperty("screenTitle", True)
        self.screenlabel1.setAlignment(Qt.AlignCenter)
        encryptiongrid.addWidget(self.screenlabel1, 0, 0)

        self.screenlabel1 = QLabel("Let's make some key pairs!")
        self.screenlabel1.setProperty("screenDescription", True)
        self.screenlabel1.setAlignment(Qt.AlignCenter)
        encryptiongrid.addWidget(self.screenlabel1, 1, 0)

        self.encbtn1 = QPushButton('Create Key Pair')
        self.encbtn1.setProperty("encButton", True)
        self.encbtn1.clicked.connect(self.createKeyWindow)
        encryptiongrid.addWidget(self.encbtn1, 3, 0)

        self.encbtn2 = QPushButton('List Keys')
        self.encbtn2.setProperty("encButton", True)
        self.encbtn2.clicked.connect(self.displayKeysWindow)
        encryptiongrid.addWidget(self.encbtn2, 4, 0)

        self.encbtn2 = QPushButton('Encrypt/Decrypt')
        self.encbtn2.setProperty("encButton", True)
        self.encbtn2.clicked.connect(self.selectKeyToEncryptDecryptText)
        encryptiongrid.addWidget(self.encbtn2, 5, 0)

        vbox2.addLayout(encryptiongrid)

        hbox = QHBoxLayout()
        hbox.addLayout(vbox1)
        hbox.addLayout(vbox2)

        self.setLayout(hbox)

        self.setStyleSheet("QWidget {max-width: 720px; max-height: 480px;} \
        *[navgridwidget=\"true\"] {max-width: 240px; max-height: 480px; \
        background-color: rgb(196,196,196);} *[screen=\"true\"] {max-width: \
        480px; min-height: 480px; max-height: 480px; background-color: \
        rgb(119,33,98);} *[navTitle=\"true\"] {background-color:\
        rgb(119,33,98); font-weight: bold; font-size: 20px; color: white}\
        *[screengridwidget=\"true\"] {max-width: 480px; max-height: 480px; \
        min-width: 480px; min-height: 480px;\
        background-color: rgb(119,33,98);} *[screenTitle=\"true\"] \
        {font-weight: bold; font-size: 20px; color: white}\
        *[screenDescription=\"true\"] {color: white} *[encButton=\"true\"] \
        {max-width: 200px; max-height: 50px; margin-left: 140px}")

        self.setGeometry(300, 150, 720, 480)
        self.move(300, 150)
        self.setWindowTitle('Open Source Buddy')
        self.show()


    @staticmethod
    def showDevelopMessage(label='', title=''):
        """
        To show a popup window with some information or error message.
        """
        messageDialogue = QDialog()
        navgrid = QGridLayout()
        messageDialogue.setLayout(navgrid)

        if label:
            message = QLabel(label)
            messageDialogue.setWindowTitle(title)
        else:
            message = QLabel("This feature is under development. "
                             "Please check back later.")
            messageDialogue.setWindowTitle("Under Development")
        message.setStyleSheet("QLabel {font-size: 18px; padding: 10px}")
        navgrid.addWidget(message, 0, 0)

        btn = QPushButton('Okay')
        btn.setStyleSheet("QPushButton {max-width: 100px; margin-left: 200px;\
        margin-bottom: 20px; padding: 5px}")
        navgrid.addWidget(btn, 2, 0)

        def closeWindow():
            """
            Just to close this Dialogue
            """
            messageDialogue.close()

        btn.clicked.connect(closeWindow)

        messageDialogue.setWindowModality(Qt.ApplicationModal)
        messageDialogue.setGeometry(400, 320, 400, 100)
        messageDialogue.exec_()


    def createKeyWindow(self):
        """
        To create popup window which can help create Key Pairs.
        """
        createKeysDialogue = QDialog()
        createKeysGrid = QGridLayout()
        createKeysDialogue.setLayout(createKeysGrid)

        nameLabel = QLabel("Name")
        nameLabel.setStyleSheet("QLabel {font-size: 14px; font-weight:\
        bold; margin-left: 20px; margin-top: 30px; margin-bottom: 10px;\
        padding-right: 0px; margin-right: 0px; max-width: 100px;\
        padding-top: -20px}")
        createKeysGrid.addWidget(nameLabel, 1, 0)

        self.nameEdit = QLineEdit()
        self.nameEdit.setStyleSheet("QLineEdit {min-width: 150px;\
         margin-left: 0px;}")
        createKeysGrid.addWidget(self.nameEdit, 1, 1)

        emailLabel = QLabel("Email")
        emailLabel.setStyleSheet("QLabel {font-size: 14px;\
        font-weight: bold; margin-left: 20px; max-width: 100px;\
        padding-top: 5px}")
        createKeysGrid.addWidget(emailLabel, 2, 0)

        self.emailEdit = QLineEdit()
        self.emailEdit.setStyleSheet("QLineEdit {min-width: 150px;\
        margin-left: 0px;}")
        createKeysGrid.addWidget(self.emailEdit, 2, 1)

        passLabel = QLabel("Passphrase")
        passLabel.setStyleSheet("QLabel {font-size: 14px; font-weight:\
        bold; margin-left: 20px; margin-bottom: 30px; max-width: 100px;\
        padding-top: 30px}")
        createKeysGrid.addWidget(passLabel, 3, 0)

        self.passEdit = QLineEdit()
        self.passEdit.setStyleSheet("QLineEdit {min-width: 150px;\
        margin-left: 0px;}")
        createKeysGrid.addWidget(self.passEdit, 3, 1)

        resetBtn = QPushButton('Reset')
        resetBtn.setStyleSheet("QPushButton {max-width: 100px;\
        padding: 5px}")
        createKeysGrid.addWidget(resetBtn, 4, 0)
        resetBtn.clicked.connect(self.createKeyReset)

        createKeyBtn = QPushButton('Create')
        createKeyBtn.setStyleSheet("QPushButton {max-width: 100px;\
        padding: 5px; margin-left: 150px;}")
        createKeysGrid.addWidget(createKeyBtn, 4, 1)
        createKeyBtn.clicked.connect(self.createKeyCreate)

        createKeysDialogue.setWindowTitle("Create Key Pair")
        createKeysDialogue.setWindowModality(Qt.ApplicationModal)
        createKeysDialogue.setGeometry(250, 200, 400, 200)
        createKeysDialogue.exec_()


    def createKeyReset(self):
        """
        Resetting values of input field in Create Key Pair form
        """
        self.nameEdit.setText('')
        self.emailEdit.setText('')
        self.passEdit.setText('')


    def createKeyCreate(self):
        """
        Validate user entered values and create key pair after successful
        validation.
        """
        if self.nameEdit.text():
            name = self.nameEdit.text()
        else:
            self.showDevelopMessage("Please enter the Name", "Error")
            self.createKeyReset()
            return
        if self.regex.match(self.emailEdit.text()):
            email = self.emailEdit.text()
        else:
            self.showDevelopMessage("Please enter the proper Email "\
                                    "address", "Error")
            self.createKeyReset()
            return
        if self.passEdit.text():
            passphrase = self.passEdit.text()
        else:
            self.showDevelopMessage("Please enter Passphrase", "Error")
            self.createKeyReset()
            return

        inputData = self.gpg.gen_key_input(name_real=name,
                                           name_email=email,
                                           passphrase=passphrase)
        key = self.gpg.gen_key(inputData)
        if key:
            self.showDevelopMessage("Key Pair creation successful."\
                                    " Checkout your key from 'List."\
                                    " Keys' option", "Success")
            self.createKeyReset()
        else:
            self.showDevelopMessage("Please try again", "Error")
            self.createKeyReset()


    def exportPublicKey(self):
        """
        To display Public Key with respect to keyid.
        """
        sender = self.sender()
        publicKey = self.gpg.export_keys(sender.text())
        self.showDevelopMessage(publicKey, "Public Key!")


    def displayKeysWindow(self):
        """
        Popup window to display keys available after creation.
        """
        displayKeysDialogue = QDialog()
        displayKeysGrid = QGridLayout()
        displayKeysDialogue.setLayout(displayKeysGrid)

        keys = self.gpg.list_keys()
        if keys:
            message = QLabel("S.No.")
            message.setStyleSheet("QLabel {font-weight: bold; font-size:\
            12px; border: 1px solid black;}")
            displayKeysGrid.addWidget(message, 0, 0)

            message = QLabel("Uids")
            message.setStyleSheet("QLabel {font-weight: bold; font-size:\
            12px; border: 1px solid black;}")
            displayKeysGrid.addWidget(message, 0, 1)

            message = QLabel("Fingerprint")
            message.setStyleSheet("QLabel {font-weight: bold; font-size:\
            12px; border: 1px solid black;}")
            displayKeysGrid.addWidget(message, 0, 2)

            message = QLabel("Key ID")
            message.setStyleSheet("QLabel {font-weight: bold; font-size:\
            12px; border: 1px solid black;}")
            displayKeysGrid.addWidget(message, 0, 3)

            message = QLabel("Created On")
            message.setStyleSheet("QLabel {font-weight: bold; font-size:\
            12px; border: 1px solid black;}")
            displayKeysGrid.addWidget(message, 0, 4)

            message = QLabel("Expires On")
            message.setStyleSheet("QLabel {font-weight: bold; font-size:\
            12px; border: 1px solid black;}")
            displayKeysGrid.addWidget(message, 0, 5)

            for x, y in enumerate(keys):
                del y
                message = QLabel(str(x + 1))
                message.setStyleSheet("QLabel {font-size: 12px;}")
                displayKeysGrid.addWidget(message, x + 1, 0)

                message = QLabel(' '.join(keys[x]["uids"]))
                message.setStyleSheet("QLabel {font-size: 12px;}")
                displayKeysGrid.addWidget(message, x + 1, 1)

                message = QLabel(keys[x]["fingerprint"])
                message.setStyleSheet("QLabel {font-size: 12px;}")
                displayKeysGrid.addWidget(message, x + 1, 2)

                showKey = QPushButton(keys[x]["keyid"])
                showKey.setStyleSheet("QPushButton {font-size: 12px;}")
                displayKeysGrid.addWidget(showKey, x + 1, 3)
                showKey.clicked.connect(self.exportPublicKey)

                if keys[x]["date"]:
                    created = keys[x]["date"]
                    createdOn = datetime.fromtimestamp(int(created)).\
                    strftime('%Y-%m-%d %H:%M:%S')
                else:
                    createdOn = "Not Present"
                message = QLabel(createdOn)
                message.setStyleSheet("QLabel {font-size: 12px;}")
                displayKeysGrid.addWidget(message, x + 1, 4)

                if keys[x]["expires"]:
                    expires = keys[x]["expires"]
                    expiresOn = datetime.fromtimestamp(int(expires)).\
                    strftime('%Y-%m-%d %H:%M:%S')
                else:
                    expiresOn = "Not Defined"
                message = QLabel(expiresOn)
                message.setStyleSheet("QLabel {font-size: 12px;}")
                displayKeysGrid.addWidget(message, x + 1, 5)
        else:
            message = QLabel("No Keys found. "
                             "Use 'Create Key Pairs'.")
            message.setStyleSheet("QLabel {font-size: 18px; padding:\
            10px}")
            displayKeysGrid.addWidget(message, 0, 0)

            btn = QPushButton('Okay')
            btn.setStyleSheet("QPushButton {max-width: 100px;\
            margin-left: 120px; margin-bottom: 20px; padding: 5px}")
            displayKeysGrid.addWidget(btn, 2, 0)

            def closeWindow():
                """
                Just to close this Dialogue
                """
                displayKeysDialogue.close()

            btn.clicked.connect(closeWindow)

        displayKeysDialogue.setWindowTitle("List of Key Pairs")
        displayKeysDialogue.setWindowModality(Qt.ApplicationModal)
        displayKeysDialogue.setGeometry(250, 200, 600, 200)
        displayKeysDialogue.exec_()


    def selectKeyToEncryptDecryptText(self):
        """
        Popup window to encrypt/decrypt text for/using respectively.
        """
        displayKeysDialogue = QDialog()
        keysToEncryptTextGrid = QGridLayout()
        displayKeysDialogue.setLayout(keysToEncryptTextGrid)

        keys = self.gpg.list_keys()
        if keys:
            message = QLabel("Click below to encrypt text\n for Private"\
                             "Key using available\n Public Key")
            message.setStyleSheet("QLabel {font-weight: bold; font-size:\
            12px;}")
            message.setAlignment(Qt.AlignCenter)
            keysToEncryptTextGrid.addWidget(message, 0, 3)

            message = QLabel("Click below to decrypt text\n using "\
                             "available Private Key\n to us")
            message.setStyleSheet("QLabel {font-weight: bold; font-size:\
            12px;}")
            message.setAlignment(Qt.AlignCenter)
            keysToEncryptTextGrid.addWidget(message, 0, 4)

            finalGridX = 0

            for x, y in enumerate(keys):
                del y
                message = QLabel(str(x + 1) + ".")
                message.setStyleSheet("QLabel {font-size: 12px;}")
                keysToEncryptTextGrid.addWidget(message, x + 1, 1)

                message = QLabel(' '.join(keys[x]["uids"]))
                message.setStyleSheet("QLabel {font-size: 12px;}")
                keysToEncryptTextGrid.addWidget(message, x + 1, 2)

                encryptFor = QPushButton("pub"+keys[x]["keyid"])
                encryptFor.setStyleSheet("QPushButton {font-size: 12px;}")
                keysToEncryptTextGrid.addWidget(encryptFor, x + 1, 3)
                encryptFor.clicked.connect(self.encryptText)

                decryptWith = QPushButton(keys[x]["keyid"])
                decryptWith.setStyleSheet("QPushButton {font-size: 12px;}")
                keysToEncryptTextGrid.addWidget(decryptWith, x + 1, 4)
                decryptWith.clicked.connect(self.decryptText)

                finalGridX = x

            message = QLabel(" ")
            message.setStyleSheet("QLabel {margin-top: 50px;}")
            keysToEncryptTextGrid.addWidget(message, finalGridX + 1, 1)
        else:
            message = QLabel("No Keys found. "
                             "Use 'Create Key Pairs'.")
            message.setStyleSheet("QLabel {font-size: 18px; padding:\
            10px}")
            keysToEncryptTextGrid.addWidget(message, 0, 0)

            btn = QPushButton('Okay')
            btn.setStyleSheet("QPushButton {max-width: 100px;\
            margin-left: 120px; margin-bottom: 20px; padding: 5px}")
            keysToEncryptTextGrid.addWidget(btn, 2, 0)

            def closeWindow():
                """
                Just to close this Dialogue
                """
                displayKeysDialogue.close()

            btn.clicked.connect(closeWindow)

        displayKeysDialogue.setWindowTitle("Encrypt/Decrypt Text")
        displayKeysDialogue.setWindowModality(Qt.ApplicationModal)
        displayKeysDialogue.setGeometry(250, 200, 600, 200)
        displayKeysDialogue.exec_()


    def encryptText(self):
        """
        Encrypt text using the text given by user!
        """
        self.keyToUseForEncryption = self.sender().text()
        displayKeysDialogue = QDialog()
        keysToEncryptTextGrid = QGridLayout()
        displayKeysDialogue.setLayout(keysToEncryptTextGrid)

        self.textToEncrypt = QPlainTextEdit()
        self.textToEncrypt.document().setPlainText("Write Encrypted"\
                                                   " text here!")
        self.textToEncrypt.textChanged.connect(self.triggerEncryptText)
        self.textToEncrypt.setStyleSheet("QPlainTextEdit {min-width:\
        150px; margin-left: 0px; min-height: 300px;}")
        keysToEncryptTextGrid.addWidget(self.textToEncrypt, 0, 0, 0, 4)

        self.encryptedText = QPlainTextEdit()
        self.encryptedText.document().setPlainText("Decrypted text will"\
                                                   " be displayed here!")
        self.encryptedText.setStyleSheet("QPlainTextEdit {min-width:\
        150px; margin-left: 0px; min-height: 300px;}")
        keysToEncryptTextGrid.addWidget(self.encryptedText, 0, 4, 0, 6)

        displayKeysDialogue.setWindowTitle("Encrypt Text!")
        displayKeysDialogue.setWindowModality(Qt.ApplicationModal)
        displayKeysDialogue.setGeometry(250, 200, 600, 200)
        displayKeysDialogue.exec_()

    def triggerEncryptText(self):
        """
        Converting Plain Text to Encrypted text using Public Key.
        """
        afterEncryption = self.gpg.encrypt(
            self.textToEncrypt.toPlainText(),
            self.keyToUseForEncryption.lstrip('pub'))
        self.encryptedText.document()\
            .setPlainText(afterEncryption.data.decode())

    def decryptText(self):
        """
        Decrypt text using the key given by user!
        """
        self.keyToUseForEncryption = self.sender().text()
        displayKeysDialogue = QDialog()
        decryptTextGrid = QGridLayout()
        displayKeysDialogue.setLayout(decryptTextGrid)

        message = QLabel("Enter Passphrase")
        message.setStyleSheet("QLabel {font-weight: bold; font-size:\
        12px;}")
        decryptTextGrid.addWidget(message, 0, 0)

        self.passPhraseEdit = QLineEdit()
        self.passPhraseEdit.textChanged.connect(self.triggerDecryptText)
        self.passPhraseEdit.setStyleSheet("QLineEdit {min-width: 150px;\
        margin-left: 0px;}")
        decryptTextGrid.addWidget(self.passPhraseEdit, 0, 2)

        self.textToDecrypt = QPlainTextEdit()
        self.textToDecrypt.document().setPlainText("Write your text"\
                                                   " here!")
        self.textToDecrypt.textChanged.connect(self.triggerDecryptText)
        self.textToDecrypt.setStyleSheet("QPlainTextEdit {min-width:\
        150px; margin-left: 0px; min-height: 220px; max-height: 220px;}")
        decryptTextGrid.addWidget(self.textToDecrypt, 1, 0, 1, 4)

        self.decryptedText = QPlainTextEdit()
        self.decryptedText.document().setPlainText("Encrypted text will"\
                                                   " be displayed here!")
        self.decryptedText.setStyleSheet("QPlainTextEdit {min-width:\
        150px; margin-left: 0px; min-height: 220px; max-height: 220px;}")
        decryptTextGrid.addWidget(self.decryptedText, 1, 4, 1, 6)

        displayKeysDialogue.setWindowTitle("Decrypt Text!")
        displayKeysDialogue.setWindowModality(Qt.ApplicationModal)
        displayKeysDialogue.setGeometry(250, 200, 600, 320)
        displayKeysDialogue.exec_()


    def triggerDecryptText(self):
        """
        Converting Encrypted Text to Plain Text text using Private Key.
        """
        passphrase = self.passPhraseEdit.text()
        if passphrase:
            afterDecryption = self.gpg.decrypt(
                self.textToDecrypt.toPlainText(),
                passphrase=passphrase)
            if afterDecryption.ok:
                self.decryptedText.document()\
                    .setPlainText(afterDecryption.data.decode())
            else:
                self.decryptedText.document()\
                    .setPlainText(afterDecryption.status)
        else:
            self.decryptedText.document()\
                .setPlainText("Please enter Passphrase")



if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = OpenSourceBuddy()
    sys.exit(app.exec_())
